

import java.util.Vector;
import java.util.EnumSet;

enum STATUS{
  DEAD,
  STATIC,
  WALKING,
  RUNNING,
  JUMPING,
  OVERWHELMING,
  INTERACTIVE,
  NOT_INTERACTIVE
}

enum PLAYER{
  PLAYER,
  SPECATOR
}

enum STAGE{
  GAME,
  SELECTION,
  MENU
}

public enum CAT{
  BLACK("Black", "b", 3),
  COLORFUL("Colorful", "c", 3),
  YELLOW("Yellow", "y", 3);

  private final String full;
  private final String abbr;
  private final int frames;

  private CAT(String full, String abbr, int frames){
    this.full = full;
    this.abbr = abbr;
    this.frames = frames;
  }

  public String getFullName(){
    return full;
  }

  public String getAbbreviatedName(){
    return abbr;
  }

  public int getFrames(){
    return frames;
  }

  private static CAT[] values = null;

  public static CAT getCatEnum(int i) {
    if(CAT.values == null) {
        CAT.values = CAT.values();
    }
    return CAT.values[i];
  }
}

public static int W = 900;
public static int H = 260;



Vector<Camera> activeCameras;
Vector<Object> objects;
Vector<Cat> cats;

class Display{

}

class Player{
  int player;
  int playerCat;
  int playerNumber;

  void initialize(int _player, int _playerNumber, int _playerCat){
    player = _player;
    playerNumber = _playerNumber;
    playerCat = _playerCat;
  }

  int isPlayer(){
    return player;
  }
}


class Misc{

  float cats_max_x(){
    float max = cats.get(0).position.x;
    for(int i=1; i<CATS; i++){
      if(cats.get(0).position.x > max){
        max = cats.get(0).position.x;
      }
    }
    return max;
  }
  float cats_min_x(){
    float min = cats.get(0).position.x;
    for(int i=1; i<CATS; i++){
      if(cats.get(0).position.x < min){
        min = cats.get(0).position.x;
      }
    }
    return min;
  }
  float cats_max_y(){
    float max = cats.get(0).position.y;
    for(int i=1; i<CATS; i++){
      if(cats.get(0).position.y > max){
        max = cats.get(0).position.y;
      }
    }
    return max;
  }
  float cats_min_y(){
    float min = cats.get(0).position.y;
    for(int i=1; i<CATS; i++){
      if(cats.get(0).position.y < min){
        min = cats.get(0).position.y;
      }
    }
    return min;
  }
}


//maybe enum with cat information?
/*
public enum States {
  ...
  MASSACHUSETTS("Massachusetts",  "MA",   true),
  MICHIGAN     ("Michigan",       "MI",   false),
  ...; // all 50 of those

  private final String full;
  private final String abbr;
  private final boolean originalColony;

  private States(String full, String abbr, boolean originalColony) {
      this.full = full;
      this.abbr = abbr;
      this.originalColony = originalColony;
  }

  public String getFullName() {
      return full;
  }

  public String getAbbreviatedName() {
      return abbr;
  }

  public boolean isOriginalColony(){
      return originalColony;
  }
}
*/


int pause = 0;
int STAGE = 2;
Stage [] stages;
PImage [] catImages;
Vector<Item> items;
int CATS = 3;
int CW = 40;
int CH = 40;
public static final float STARTX = 0;
public static final float STARTY = 90;
public static final int FRAME = 3;
public static final float UNITDISTANCE = 30;
int WINNER = -1;
String WIN;
int stage;
Misc misc;

Camera actionCamera;
Camera fieldCamera;
Camera activeCamera;
Camera testCamera;

class Animation{
  int frame;
  int frames;
  PImage [] sprite;

  void initialize(int _frames){
    frames = _frames;
    sprite = new PImage[frames];
  }

  void setFrame(int _frames, PImage _sprite){
    sprite[_frames] = _sprite;
  }
}
class Environment extends Object{

}


class Object{
  PVector position;
  PVector velocity;
  PVector size;
  Animation animation;
  EnumSet<STATUS> statuses;

  void initialize(){
    position = new PVector();
    velocity = new PVector();
    size = new PVector();
    animation = new Animation();
  }

  void initialize(float _x, float _y, int _xsize, int _ysize, EnumSet<STATUS> _statuses){
    position = new PVector(_x, _y);
    velocity = new PVector();
    size = new PVector(_xsize, _ysize);
    animation = new Animation();
    statuses = _statuses;
  }
}

class Camera extends Object{

  Object target;

  float zoom = 1;
  float rotation = 0;
  int x_priority;
  float x_max, x_min;
  float y_max, y_min;

  Camera(Object _target){
    target = _target;
  }
  Camera(){
    target = null;
  }
  void setTarget(Object _target){
    target = _target;
  }
  void followTarget(){
    if(target != null){
      x_max = target.position.x + 100;
      x_min = target.position.x - 100;

      y_max = target.position.y + 100;
      y_min = target.position.y - 100;

      position.x = (x_max+x_min) / 2;
      position.y = (y_max+y_min) / 2;

      rotation = sin(frameCount/100.0);
    }
    else if(target == null){
      x_max = misc.cats_max_x()+100;
      x_min = misc.cats_min_x()-100;
      y_max = misc.cats_max_y();
      y_min = misc.cats_min_y();

      position.x = (x_max+x_min) / 2;
      position.y = (y_max+y_min) / 2;
    }
  }
  void drawObject(Object object, int frame){
    if((x_max-x_min)/W*H <= (y_max-y_min)){
      x_priority = 1;
      zoom = W/(x_max-x_min);
    }
    else{
      x_priority = 0;
      zoom = H/(y_max-y_min);
    }
    pushMatrix();
    translate(W/2, H/2);
    rotate(rotation);
//    println("camera position: ", position.x, ", "+position.y);
    println("camera zoom: ", zoom);
//    println("object_x: ",object.position.x-position.x);
//    println("object_y: ",object.position.y-position.y);
//    println("object_frame: ", frame);
    println(frame);
    float coordX = (object.position.x)-(position.x);
    float coordY = (object.position.y)-(position.y);
    float sizeX = (object.size.x*zoom);
    float sizeY = (object.size.y*zoom);

    println(coordX, coordY, zoom, sizeX, sizeY);
    image(object.animation.sprite[frame], (object.position.x)-(position.x), (object.position.y)-(position.y),object.size.x*zoom,object.size.y*zoom);
    //image(object.animation.spritep);
//   image(object.animation.frames[frame], (object.position.x)-(position.x), (object.position.y)-(position.y),object.size.x, object.size.y);
    popMatrix();

  }
}

class Item extends Object{
}

class Cat extends Object{

  Vector<Skill> skills;

  void initialize(){
    position = new PVector();
    velocity = new PVector();
    size = new PVector();
    animation = new Animation();
    skills = new Vector<Skill>();
  }

  void initialize(float _x, float _y, int _xsize, int _ysize, EnumSet<STATUS> _statuses){
    position = new PVector(_x, _y);
    velocity = new PVector();
    size = new PVector(_xsize, _ysize);
    animation = new Animation();
    skills = new Vector<Skill>();
    statuses = _statuses;
  }
}

class Stage{
  int stageNumber;

  Stage(int _stage){
    stageNumber = _stage;
  }
  void run(){
    if(stageNumber == 0){

    }
    else if(stageNumber == 1){
      background(255);

      for (Cat cat : cats){
        int frame = int(((cat.position.x)-STARTX)/UNITDISTANCE*FRAME)%FRAME;
        cat.animation.frame = frame;
        //activeCamera.followTarget(cats[1]);

        cat.velocity.x=abs(3*sin(cat.position.x))+abs(random(5));
        cat.position.x += cat.velocity.x;

        if (cat.position.x > 900 && WINNER == -1){
          //WINNER = i+1;
          WINNER = 1;
          WIN = "Cat " + WINNER + " wins!";
        }
      }

      if(WINNER != -1){
        textSize(32);
        fill(0, 102, 153, 204);
        text(WIN, W/3, H/3, -30);
      }
/*
      //Cameras draw objects
      for(Camera activeCamera : activeCameras){
        activeCamera.followTarget();
        for(Object object : objects){
          activeCamera.drawObject(object, object.animation.frame);
        }
        for(Cat cat : cats){
          activeCamera.drawObject(cat, cat.animation.frame);
        }
      }
*/
testCamera.followTarget();
for(Cat cat : cats){
  testCamera.drawObject(cat, cat.animation.frame);
  println(testCamera.position.x);
}

    }
  }
}

class Interface extends Object{

}

Interface pauseMenu;

void settings(){
  size(W,H);
}

void setup(){

  Vector<Object> objects = new Vector<Object>();

  //interface
  imageMode(CENTER);
  pauseMenu = new Interface();
  frameRate(45);
  misc = new Misc();

  stages = new Stage[STAGE];
  for(int i=0; i<STAGE; i++){
    stages[i] = new Stage(i);
  }
  stage = 1;

  items = new Vector<Item>();

  cats = new Vector<Cat>();
  catImages = new PImage[FRAME];

  for(int i=0; i<CATS; i++){
    CAT CATINFO = CAT.getCatEnum(i);
    Cat cat = new Cat();
    cat.initialize();

    cat.animation.initialize(CATINFO.getFrames());
    for(int j=0; j<CATINFO.getFrames(); j++){
      String path = CATINFO.getAbbreviatedName() + "-" + (j+1) + ".png";
      catImages[j] = loadImage(path); //May make an issue. Take a look at (j+1) numbering
      cat.animation.setFrame(j, catImages[j]);
      cat.size.x = CW;
      cat.size.y = CH;
    }
      cats.addElement(cat);
  }

  Vector<Camera> activeCameras = new Vector<Camera>();
  activeCameras.addElement(new Camera(null));
  activeCameras.addElement(new Camera());
  testCamera = new Camera(cats.get(0));

  testCamera.initialize();
}



void draw(){

  if(pause == 1){

  }
  else{
    stages[stage].run();
  }
}
